{!! Form::model($product,['route' => ['product.update',$id],                
                            'files' => 'true'
                        ]) !!}
	@csrf
	@method('PUT')
	Product Name: <input type="text" name="product_name" value="{{ $product->product_name }}" required="" /><br>
	Product Description<textarea name="product_description" required="">{{ $product->product_description }}</textarea><br>

	@if(isset($variants))

	@foreach($variants as $key=>$value)
	<br>
	{{ $key+1 }} Size : <input type="text" name="update_size[{{$value->id}}]" value="{{ $value->size }}"  /><br>
	{{ $key+1 }} Color: <input type="text" name="update_color[{{$value->id}}]" value="{{ $value->color }}" /><br>
	{{ $key+1 }} Price: <input type="text" name="update_price[{{$value->id}}]" value="{{ $value->variant_price }}" /><br>
	{{ $key+1 }} Image: <input type="file" name="update_image[{{$value->id}}]"  /><br><br>
	<input type="hidden" name="update_old_image[{{ $value->id }}]" value="{{ $value->variant_image }}">
	<img src={{ asset("storage/".$value->variant_image) }} height=100 width=100 /><br><br>
	@endforeach
	@endif
	<br>
	<br>
	Size : <input type="text" name="size" value=""  /><br>
	Color: <input type="text" name="color" value="" /><br>
	Price: <input type="text" name="price" value="" /><br>
	Image: <input type="file" name="image"  /><br>


<input type="submit" name="submit" value="Save">

{!! Form::close() !!}
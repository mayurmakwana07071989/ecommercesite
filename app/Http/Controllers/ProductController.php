<?php

namespace App\Http\Controllers;

use App\Product;
use App\Variants;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Builder $builder)
    {
        
        try{
            $products_get = Product::latest();
            



            if (request()->ajax()) {
                return DataTables::of($products_get->get())
                ->addIndexColumn()
                ->editColumn('action',function(Product $product){
                    

                     return '<a class="ml-2" href='.route('product.edit',[$product->id]).'><i class="fa fa-pencil"></i></a>';            
                })
                ->rawColumns(['action'])
                ->make(true);
            }
            $html = $builder->columns([
                ['defaultContent' => '','data' => 'DT_RowIndex','name' => 'DT_RowIndex','title' => '#','render' => null,'orderable' => false,'searchable' => false,'exportable' => false,'printable' => true,'width'=>'1%'],
                ['data' => 'product_name', 'name'    => 'product_name', 'title' => 'Product Name','width'=>'15%'],
                ['data' => 'product_description', 'name'    => 'product_description', 'title' => 'Product Description','width'=>'15%'],
                                ['data' => 'action', 'name'    => 'action', 'title' => 'action','width'=>'15%'],
                
            ])               
            
            ->parameters([
                'order' => [],
                'processing'    => true,
                'paging'        => true,
                'info'          => true,
                'searchDelay'   => 350,
                //'dom'           => 'Bfrtip',
                //'bFilter'       => false,
                //'sDom'          => 'lfrtip',
                //'buttons'       => [ 'reset', 'reload'],
                //'searching'   => true,
                ]);

            return view('product.index',compact('html','page'));
        }catch(Exception $e){
            return redirect()->back()->with([
                'status'    => 'error',
                'title'     => 'Error!!',
                'message'   => $e->getMessage()
                ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Product::create(['product_name'=>$request->product_name,'product_description'=>$request->product_description]);

        toastr()->success('Product addded successfully');
        return redirect('product');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $variants = Variants::where('product_id',$id)->get();
        return view('product.edit',compact('product','id','variants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
         Product::where('id',$id)->update(['product_name'=>$request->product_name,'product_description'=>$request->product_description]);

        
            $files = $request->file('update_image');

         if($request->hasFile('update_image')){
                foreach ($files as $key => $file) {


                    $extension = $file->getClientOriginalExtension();
                    $filename = Str::random(10).'.'.$extension;

                    if(!empty($filename))
                    {
                            Storage::disk('public')->put('/'.$filename,  File::get($file));
                            Variants::where('id',$key)->update(['variant_image'=>$filename]);
                    }else{
                        $update_old_image = $request->update_old_image;
                        Variants::where('id',$key)->update(['variant_image'=>$update_old_image[$key]]);
                    }
                    

                }
               
            }


         
        $update_size = $request->update_size;
        foreach ($update_size as $key => $value) {
            $color = $request->update_color;
            $update_price = $request->update_price;
           Variants::where('id',$key)->update(
            ['size'=>$update_size[$key],
            'color'=>$color[$key],            
            'variant_price'=>$update_price[$key]
            ]);
         }

if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = Str::random(10).'.'.$extension;
                Storage::disk('public')->put('/'.$filename,  File::get($file));
            }else{
                
                    $filename = 'avatar.png';
                
            }
            if(isset($request->size) && isset($request->color) && isset($request->price))
            {
         Variants::updateOrCreate(['product_id'=>$id,'size'=>$request->size,'color'=>$request->color],['size'=>$request->size,'color'=>$request->color,'variant_image'=>$filename,'variant_price'=>$request->price]);
         }
         return redirect('product'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}

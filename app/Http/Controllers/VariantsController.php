<?php

namespace App\Http\Controllers;

use App\Variants;
use Illuminate\Http\Request;

class VariantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function show(Variants $variants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function edit(Variants $variants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variants $variants)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variants $variants)
    {
        //
    }
}

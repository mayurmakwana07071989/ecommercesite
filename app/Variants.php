<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variants extends Model
{
    protected $table = 'variants';
    protected $fillable = ['product_id','size','color','variant_price','variant_image'];
}
